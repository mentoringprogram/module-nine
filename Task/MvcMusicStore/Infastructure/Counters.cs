﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PerformanceCounterHelper;

namespace MvcMusicStore.Infastructure
{
    // attribute to make performance counter work with the app
    [PerformanceCounterCategory("MvcMusicStore", System.Diagnostics.PerformanceCounterCategoryType.MultiInstance, "MvcMusicStore")]
    public enum Counters
    {
        // COunters consists of enum values for different types of performance counting
        //Counters for home
        [PerformanceCounter("Go Home counter", "Go Home", System.Diagnostics.PerformanceCounterType.NumberOfItems32)]
        GoToHome,
        // Counters for log out
        [PerformanceCounter("Successful LogOut counter", "Go LogOut", System.Diagnostics.PerformanceCounterType.NumberOfItems32)]
        SuccessfulLogOut,
        // Counter for log in
        [PerformanceCounter("Successful LogIn counter", "Go LogIn", System.Diagnostics.PerformanceCounterType.NumberOfItems32)]
        SuccessfulLogIn,
    }
}
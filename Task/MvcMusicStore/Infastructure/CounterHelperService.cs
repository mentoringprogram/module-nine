﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PerformanceCounterHelper;

namespace MvcMusicStore.Infastructure
{
    /// <summary>
    /// Service tha creates counter and increments it
    /// </summary>
    public class CounterHelperService
    {
        // use lazy loading for optimization
        private static readonly Lazy<CounterHelperService> lazy =
            new Lazy<CounterHelperService>(() => new CounterHelperService());

        private CounterHelper<Counters> CounterHelper;

        public static CounterHelperService Instance
        {
            get { return lazy.Value; }
        }

        private CounterHelperService()
        {
            // First we need to install types of counters
            PerformanceHelper.Install(typeof(Counters));
            // Then we create counters
            CounterHelper = PerformanceHelper.CreateCounterHelper<Counters>("CounterHelper");
        }

        public void Increment(Counters counter)
        {
            CounterHelper.Increment(counter);
        }
    }
}